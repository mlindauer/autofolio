'''
cf_wrapper_inner.py -- wrapper of claspfolio

@author:     Marius Lindauer
        
@copyright:  2014 Potassco. All rights reserved.
        
@license:    GPL

@contact:    manju@cs.uni-potsdam.de
'''

#standard imports
import sys
import os
import json

# own imports
import parameter_parser
from tempfile import NamedTemporaryFile, mkdtemp
from subprocess import Popen
import traceback
import shutil
import datetime

class ClaspfolioWrapper(object):
    
    def __init__(self):
        ''' Constructor '''
        self._config_file = ""
        self._instance = ""
        self.cutoff = 1.0 # at most 1.0 second
        self._cutoff_length = -1 #unused
        self.seed = -1
        self._params = []
        self._config = {}
        self._thread_to_params = {}
        self._thread_to_solver = {}
        self._MAX_MEM = 4000
        self._VERBOSE = False
        
        self.status = "TIMEOUT"
        self.time = self.cutoff
        self.quality = sys.maxint
        
        self.misc = "everything ok"
        
        self.__TEMPDIR = "."
        self.__DELETE = True 
        self.__RUNSOLVER = True
    
    def parse_args(self,argv):
        ''' parse command line arguments 
            Args:
                argv: command line arguments list
        
        '''
        if(len(argv) < 5):
            print("cf_wrapper.py is a wrapper for claspfolio")
            print("Usage: python2.7 cf_wrapper.py <config_file> <instance_relname> <cutoff_time> <cutoff_length> <seed> <params to be passed on>.")
            sys.exit()
        
        if (self._VERBOSE):
            sys.stderr.write("Parsing of Commandline Arguments\n")
        
        self._config_file = argv[1]
        self._instance = argv[2]
        self.__instance_specific = argv[3] # not used
        self.cutoff = max([float(argv[4]),1.0]) # at most 1.0 second
        self._cutoff_length = float(argv[5]) #unused
        self.seed = float(argv[6])
        self._params = argv[7:]    # remaining parameters
        
        self._read_config_file()
        parameter_parser_ = parameter_parser.ParameterParser()
        [self._thread_to_params, self._thread_to_solver, params_to_tags] = parameter_parser_.parse_parameters(self._params,"--")
        

    def _read_config_file(self):
        ''' read config file (json format)
            "tmp": save path for model dir
            "runsolver": path to runsolver
        '''
        if (self._VERBOSE):
            sys.stderr.write("Read Config File\n")
        
        if os.path.isfile(self._config_file):
            fp = open(self._config_file,"r")
            self._config = json.load(fp)
        else:
            sys.stderr.write("Warning: Config file not found!")

    def start(self):
        '''
            start solver
        '''
        if (self._VERBOSE):
            sys.stderr.write("Start Clasp\n")
        globals_ = self._thread_to_params.pop(0)
        
        self.portfolio_file = None
        globals_.extend(self._thread_to_params[1])
        
        root_path = os.path.join(os.path.split(sys.argv[0])[0], "..")
        if self._config.get("tmp"):
            self.__TEMPDIR = self._config.get("tmp")
        
        claspfolio = os.path.join(root_path, "..", self._thread_to_solver[1])
        self.solver_log = NamedTemporaryFile(prefix="SolverLog", dir=self.__TEMPDIR, delete=self.__DELETE)
        if self.__RUNSOLVER:
            cmd = [self._config["runsolver"], "-W", str(self.cutoff), "-w", "/dev/null"]
        else:
            cmd = []
        
        # temporary model directory
        model_dir = mkdtemp(dir=self.__TEMPDIR)

        #TODO: Change --fold handling
        cmd.extend(["python", claspfolio,"--smac","--model-dir", model_dir])
        cmd.extend(["--test-set", "\"(%d;%s)\""%(1, self._instance)])
        cmd.append("--train-set")
        train = []
        for i in xrange(10):
            if i != int(self._instance):
                train.append("(%d;%d)" %(1, i))
        cmd.append("\"%s\"" %(",".join(train)))
        
        cmd.extend(globals_)
        #cmd_arr = cmd

        cmd = " ".join(cmd)
        print(cmd)
        #execute
        popen_ = Popen(cmd, shell=True, stdout=self.solver_log, stderr=self.solver_log)
        #popen_ = Popen(cmd_arr, stdout=self.solver_log, stderr=self.solver_log)
        popen_.communicate()
        
        self.__read_output(self.solver_log)
        
        self.solver_log.close()
        shutil.rmtree(model_dir)
        
        #if claspfolio failed, run SBS mode
        if self.status == "CRASHED":
            self.misc = "configuration crashed; runnning SBS"
            # temporary model directory
            model_dir = mkdtemp(dir=self.__TEMPDIR)
            self.solver_log = NamedTemporaryFile(prefix="SolverLog", dir=self.__TEMPDIR, delete=self.__DELETE)
            while globals_:
                head = globals_.pop(0)
                if head == "--aslib-dir":
                    dir_ = globals_.pop(0)
                    break
            
            cmd = ["python", claspfolio, "--smac","--model-dir",model_dir,"--preconf", "sbs", "--aslib-dir", dir_]
            cmd.extend(["--test-set", "\"(1;%s)\""%(self._instance)])
            cmd.append("--train-set")
            cmd.append("\"%s\"" %(",".join(train)))
            cmd = " ".join(cmd)
            print(cmd)
            popen_ = Popen(cmd, shell=True, stdout=self.solver_log, stderr=self.solver_log)
            popen_.communicate()
            self.__read_output(self.solver_log)
            #self.quality *= 10 # penalize score
            self.solver_log.close()
            shutil.rmtree(model_dir)
        
    def __read_output(self, solver_log):
        '''
            read json output of solvers
            Args:
                solver_log: file pointer of solver output
        '''
        self.time = self.cutoff
        self.status = "TIMEOUT"
        if (self._VERBOSE):
            for l in solver_log:
                print(l.replace("\n",""))
                sys.stderr.write(l.replace("\n","")+"\n")
        try:
            solver_log.seek(0)
            lines = solver_log.readlines()
            try:
                for line in lines:
                    if line.startswith("Result for SMAC:"):
                        last_line = float(line.split(":")[1])
                self.quality = float(last_line)
            except ValueError:
                last_line = lines[-2] # if python is compiled with pydebug,the last line is the number of refs
                self.quality = float(last_line)
            self.status = "SAT"
        except:
            sys.stdout.write("Timeout of claspfolio (or other problems)\n")
            self.status = "CRASHED"  
            
        try:
            solver_log.seek(0)
            for line in solver_log:
                if self.status == "CRASHED" and "Traceback"  in line: # python crashed
                    self.status = "CRASHED" 
        except:
            pass
        
if __name__ == '__main__':
    
    starttime = datetime.datetime.now()
    #os.putenv("LD_LIBRARY_PATH", "/cvos/shared/apps/python/2.7/lib:/cvos/shared/apps/gcc/4.7.0/lib:/cvos/shared/apps/gcc/4.7.0/lib64:/cvos/shared/apps/intel-tbb/em64t/30_018oss/lib:/cvos/shared/apps/torque/3.0.1/lib/:/cvos/shared/apps/maui/3.3.1/lib") 
    # zuse specific:
    try:
        wrapper = ClaspfolioWrapper()
        wrapper.parse_args(sys.argv)
        wrapper.start()   
        endtime = datetime.datetime.now()
        duration = (endtime-starttime).seconds
        print("Result for ParamILS: %s, %f, -1, %f, %d, %s" % (wrapper.status,duration,wrapper.quality,wrapper.seed,wrapper.misc))
    except:
        traceback.print_exc()
        endtime = datetime.datetime.now()
        duration = (endtime-starttime).seconds
        print("Result for ParamILS: %s, %f, -1, %d, %d, Wrapper raised exception" % ("CRASHED",duration,sys.maxint,wrapper.seed))
        wrapper.solver_log.close()
        wrapper.watcher_log.close()
        


