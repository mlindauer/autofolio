#!/usr/local/bin/python2.7
# encoding: utf-8
'''
autofolio -- algorithm configuration for algorithm selection - 

Post-processing script to assess the quality of the configured selectors

@author:     Marius Lindauer

@copyright:  2014 Marius Lindauer. All rights reserved.

@license:    GPLv3

@contact:    lindauer@cs.uni-freiburg.de    
'''

import sys
import os
import logging
import traceback
import random
import shutil
import arff # liac-arff
import json

import matplotlib
from fANOVA.fanova import FANOVA
from ablation.ablation import AblationAnalysis
from job_systems.bwuni_vl import BWUniVeryLongSubmitter

matplotlib.use("Agg")

from argparse import ArgumentParser
from argparse import ArgumentDefaultsHelpFormatter
from argparse import SUPPRESS

import matplotlib
matplotlib.use("pdf")

from aslib.aslib_parser import ASlibParser
from selector.claspfolio import ClaspfolioSpecifics
from selector.flexfolio import FlexfolioSpecifics
from configurator_interfaces.smac import SMAC
from stat_tests.PermutationTester import PermutationTester
from plot.scatter_plot import ScatterPlotter
from plot.overtime_plot import OverTimePlot
from param_stats.param_statistics import ParamStatistics

from job_systems.local import LocalExecutor
from job_systems.meta import MetaSubmitter
from job_systems.bwuni import BWUniSubmitter
from job_systems.orcinus import OrcinusSubmitter

__all__ = []
__version__ = 0.1
__date__ = '2014-12-08'
__updated__ = '2015-01-26'

class AutofolioPost(object):
    
    def __init__(self, root_path, args):
        ''' Constructor '''
        self.root_path = root_path
        
        self.scenario = args.scenario
        self.name = os.path.basename(self.scenario.strip("/"))
        self.selector = args.selector
        self.cutoff = args.cutoff
        
        #Configuration
        self.reps = args.repetitions
        
        self.wrapper = args.wrapper
        if not os.path.isfile(self.wrapper):
            self.wrapper = os.path.join(root_path, self.wrapper)
            if not os.path.isfile(self.wrapper):
                logging.error("Have not found target algorithm wrapper (--wrapper): %s" %(args.wrapper))
        
        self.configurator = args.configurator
        
        self._OUTER_FOLDS = 10
        self._permutations = args.permutations
        self._alpha = args.alpha
        self._fanova = args.fanova
        self._ablation = args.ablation
        
        # JOB
        self.clustersystem = args.cluster
        if self.clustersystem == "orcinus":
            self.MEMLIMIT = 800
        else:
            self.MEMLIMIT = 2000
            
        self.budget = 478800
        
        self._CSV = True
        
        self._data = {"name": self.name,
                      "configured": {
                                     "par1": None,
                                     "par10": None,
                                     "tos": None
                                     },
                      "default": {
                                     "par1": None,
                                     "par10": None,
                                     "tos": None
                                     },
                      "better" : { 
                                    "bool": None,
                                    "pvalue": None,
                                    "perms": None,
                                    "alpha" : None
                                 },
                      "training" : None,
                      "otplot": { # over time plots
                                 "medians": None,
                                 "q25": None,
                                 "q75": None,
                                 "times": None
                                 },
                      "approachDist" : None,
                      "fanova": None,
                      "ablation": None
                      }
        
    def main(self):
        ''' main method '''
        # get best configuration per inner fold
        as_parser = ASlibParser(self.scenario)
        metainfo = as_parser.get_metainfo()
        instance_dict = as_parser.get_instances()
        configs = self.get_best_()
        conf_dict, default_dict = self.evaluate_configs(configs)
        self.print_stats(conf_dict, default_dict, instance_dict, metainfo.algorithm_cutoff_time)
        
        self.get_param_stats(configs)
        
        self.do_permutation_test(conf_dict, default_dict, metainfo.algorithm_cutoff_time, perms=self._permutations, alpha=self._alpha)
        self.gen_scatter_plot(conf_dict, default_dict, metainfo.algorithm_cutoff_time)
        self.gen_time_plot()
        
        if self._fanova:
            self.get_fanova_importance()
            
        if self._ablation:
            self.submit_ablation(configs)
            
        with open("results.json", "w") as fp:
            json.dump(self._data, fp, indent=2)
            fp.flush()
    
    def get_best_(self):
        ''' get best configuration per inner fold '''
        
        base_scenario = os.path.basename(self.scenario.strip("/"))
        configs = []
        training_performance = 0
        for id_ in xrange(1,self._OUTER_FOLDS+1):
            scen_fold = "%s_f%d" %(base_scenario, id_)
            if self.configurator == "SMAC":
                smac = SMAC()
                conf, perf = smac.get_best_conf(scen_fold)
                configs.append(conf)
                training_performance += perf
                logging.debug("%d (%.1f) : %s" %(id_, perf, conf))
                
        self._data["training"] = training_performance / self._OUTER_FOLDS
        return configs
    
    def evaluate_configs(self, configs):
        ''' evaluate configurations on inner folds on test sets of outer folds '''
        all_conf_dict = {}
        all_def_dict = {}
        for fold, conf in enumerate(configs):
            if self.selector == "claspfolio":
                cf = ClaspfolioSpecifics()
                conf_dict, def_dict = cf.evaluate(conf, self.scenario, fold+1, self.wrapper, self.cutoff, 
                                                  out_conf="configured_%d.csv" %(fold+1), 
                                                  out_def="default_%d.csv" %(fold+1))
                all_conf_dict.update(conf_dict)
                all_def_dict.update(def_dict)
            elif self.selector == "flexfolio":
                cf = FlexfolioSpecifics()
                conf_dict, def_dict = cf.evaluate(conf, self.scenario, fold+1, self.wrapper, self.cutoff, 
                                                  out_conf="configured_%d.csv" %(fold+1), 
                                                  out_def="default_%d.csv" %(fold+1))
                all_conf_dict.update(conf_dict)
                all_def_dict.update(def_dict)
                
        #logging.debug(all_conf_dict)
        #logging.debug(all_def_dict)
        return all_conf_dict, all_def_dict

    def print_stats(self, conf_dict, default_dict, instance_dict, cutoff):
        ''' prints some statistics '''
        
        #remove unsolved instances wrt. to algorithm portfolio
        removed = 0
        for name, inst_ in instance_dict.iteritems():
            if "OK" not in inst_._status.values():
                del conf_dict[name]
                del default_dict[name]
                removed +=1
        logging.debug("Removed unsolved: %d" %(removed))
                
        logging.info("============================================")
        logging.info("Number of instances: %d" %(len(default_dict)))
        logging.info(">>>PAR10<<<")
        logging.info("Default: %.2f" %(sum(default_dict.values())/len(default_dict))) 
        logging.info("Configured: %.2f" %(sum(conf_dict.values())/len(conf_dict)))
        self._data["default"]["par10"] = sum(default_dict.values())/len(default_dict)
        self._data["configured"]["par10"] = sum(conf_dict.values())/len(conf_dict)
 
        logging.info(">>>Timeouts<<<")
        logging.info("Default: %d" %(len(filter(lambda x: x >= cutoff, default_dict.values()))))
        logging.info("Configured: %d" %(len(filter(lambda x: x >= cutoff, conf_dict.values()))))
        self._data["default"]["tos"] = len(filter(lambda x: x >= cutoff, default_dict.values()))
        self._data["configured"]["tos"] = len(filter(lambda x: x >= cutoff, conf_dict.values()))
 
        par1_default = map(lambda x: cutoff if x > cutoff else x, default_dict.values())
        par1_conf = map(lambda x: cutoff if x > cutoff else x, conf_dict.values())
        logging.info(">>>PAR1<<<")
        logging.info("Default: %.2f" %(sum(par1_default)/len(default_dict))) 
        logging.info("Configured: %.2f" %(sum(par1_conf)/len(conf_dict))) 
        self._data["default"]["par1"] = sum(par1_default)/len(default_dict)
        self._data["configured"]["par1"] = sum(par1_conf)/len(conf_dict)
        
        if self._CSV == True:
            with open("autofolio.csv", "w") as fp:
                fp.write("instance,autofolio\n")
                for inst, perf in conf_dict.iteritems():
                    fp.write("%s,%.2f\n" %(inst, perf))

    def get_param_stats(self, configs):
        ''' get some statistics about the parameter-value distributions '''
        stats = ParamStatistics()
        frequencies = stats.get_stats(configs)
        if frequencies.get("-@1:approach"):
            logging.debug("Selection Approach distribution:")
            logging.debug(frequencies["-@1:approach"])
            self._data["approachDist"] = frequencies["-@1:approach"]
        self._data["param_freqs"] = frequencies

    def do_permutation_test(self, conf_dict, default_dict, cutoff, perms=10000, alpha=0.05):
        ''' make a permutation test to verify that <conf_dict> statistically better than <default_dict>'''
        
        self._data["better"]["perms"] = perms
        self._data["better"]["alpha"] = alpha
        self._data["better"]["bool"] = False
        logging.info("Permutation Test (p=%d, alpha=%.2f)..." %(perms, alpha))
        tester = PermutationTester()
        rejected, switched, p_value = tester.doTest(conf_dict, default_dict, 
                                                alpha=alpha, permutations=perms, 
                                                cutoff=cutoff, par_factor=10)
        self._data["pvalue"] = p_value
        if rejected and not switched:
            logging.info("Configured is statistically better than Default (p-value: %.2f)" %(p_value))
            self._data["better"]["bool"] = True
        elif rejected and switched:
            logging.info("Default is statistically better than Configured (p-value: %.2f)" %(p_value))
        else:
            logging.info("No significant differences between Configured and Default (p-value: %.2f)" %(p_value))

    def gen_scatter_plot(self, conf_dict, default_dict, cutoff):
        ''' generate a scatter plot'''
        logging.info("Scatter Plot...%s" %(self.name+".pdf"))
        tester = PermutationTester()
        x_data, y_data = tester.extract_values_in_order(default_dict, conf_dict) 
        plotter = ScatterPlotter()
        plotter.plot_scatter_plot(x_data, y_data, labels=["Default", "Configured"], save=self.name+".pdf", 
                                  max_val=cutoff, linefactors=[2,10,100])
        
    def gen_time_plot(self):
        ''' generate a plot of training performance over time '''
        
        #get trajectories from SMAC
        logging.info("Over Time Plot...%s_overtime.pdf" %(self.name))
        trajectories = []
        for id_ in xrange(1,self._OUTER_FOLDS+1):
            scen_fold = "%s_f%d" %(self.name, id_)
            if self.configurator == "SMAC":
                smac = SMAC()
                rep_traj = smac.get_traj_perfs(scen_fold, id_)
                trajectories.append(rep_traj)
                
        plotter = OverTimePlot()
        times, medians, q25, q75 = plotter.main(trajectories,"%s_overtime.pdf"%(self.name))
        self._data["otplot"]["times"] = times
        self._data["otplot"]["medians"] = medians
        self._data["otplot"]["q25"] = q25
        self._data["otplot"]["q75"] = q75
        
    def get_fanova_importance(self):
        '''
            use fANOVA to get parameter importance
        '''
        
        logging.info("Get Parameter Importance with fAnova ...")
        fanova = FANOVA(self.root_path)
        importance = fanova.get_importance(self.name, self._OUTER_FOLDS)
        
        self._data["fanova"] = importance
        logging.info("fAnova results added to results.json")
        
    def submit_ablation(self, configs):
        '''
            use Ablation to get parameter importance; 
            since ablation is expensive we run it through our cluster interace;
            please use ablation_postprocessing.py to get the results
        '''
        logging.info("Get Parameter Importance with fAnova ...")
        ablation = AblationAnalysis(self.root_path)
        calls = ablation.generate_calls(self.name, configs)
        
        if self.clustersystem == "bwuni":
            self.clustersystem = "bwunivl"
        self.submit_job(calls)
        
        #self._data["ablation"] = importance
        #logging.info("Ablation results added to results.json")
        
    def submit_job(self, calls):
        '''
            based on <clustersystem> submit jobs (or execute it locally) 
        '''
        if self.clustersystem == "local":
            runner = LocalExecutor()
            runner.submit_jobs(calls)
        elif self.clustersystem == "meta":
            submitter = MetaSubmitter(self.root_path)
            submitter.submit_jobs(calls, self.name)
        elif self.clustersystem == "bwuni":
            submitter = BWUniSubmitter(self.budget)
            submitter.submit_jobs(calls, self.name)
        elif self.clustersystem == "bwunivl":
            submitter = BWUniVeryLongSubmitter(self.budget)
            submitter.submit_jobs(calls, self.name)
        elif self.clustersystem == "orcinus":
            submitter = OrcinusSubmitter(self.budget)
            submitter.submit_jobs(calls, self.name)
        
def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    #program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Marius Lindauer on %s.
  Copyright 2015. All rights reserved.

  Licensed under the GPLv3
  http://www.gnu.org/copyleft/gpl.html

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        
        root_path = os.path.join(os.path.split(sys.argv[0])[0], "..")
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=ArgumentDefaultsHelpFormatter)
        
        req_group = parser.add_argument_group('Required Options')
        req_group.add_argument("-s", '--scenario', required=True, help="aslib scenario")
        #parser.add_argument('--selector', default="claspfolio", choices=["claspfolio"], help="algorithm selector to configure")
        
        test_options = parser.add_argument_group("Test Options")
        test_options.add_argument("-p", "--permutations", type=int, default=100000, help="number of permutations in statistical test")
        test_options.add_argument("-a", "--alpha", type=float, default=0.05, help="significance level (alpha) in statistical test")
        test_options.add_argument("-f", "--fanova", action="store_true", default=False, help="apply fAnova to get parameter importance")
        test_options.add_argument("-A", "--ablation", action="store_true", default=False, help=SUPPRESS) #help="apply Ablation to get parameter importance")
        test_options.add_argument("-C", '--cluster', default="local", choices=["local", "meta","bwuni","orcinus"], help="cluster job submitting system where the *Ablation* runs are submitted to")
                
        conf_group = parser.add_argument_group('Configuration Options')
        conf_group.add_argument("-r", '--repetitions', type=int, default=1, help="number of configuration runs per split")
        conf_group.add_argument("-c", '--cutoff', default=3600, type=int, help="target algorithm selector runtime cutoff")
        conf_group.add_argument('--wrapper', default="wrappers/flexfolio/ff_wrapper.py", help="wrapper of algorithm selector.py")
        conf_group.add_argument('--selector', default="flexfolio", choices=["claspfolio","flexfolio"], help="selector to configure - has to compatible with wrapper")
        conf_group.add_argument('--configurator', default="SMAC", choices=["SMAC"], help="call of configurator (only SMAC is supported)")
        
        misc_group = parser.add_argument_group('MISC Options')
        misc_group.add_argument("-v", "--verbose", dest="verbose", default="INFO", choices=["INFO", "DEBUG","NOTEST"], help="set verbosity level [default: %(default)s]")
        misc_group.add_argument("-V", "--version", action='version', version=program_version_message)
        misc_group.add_argument("-S", '--seed', type=int, default=12345, help="random seed")
        
        # Process arguments
        args = parser.parse_args()
        logging.basicConfig(format="[%(levelname)s]: %(message)s", level=args.verbose)
        random.seed(args.seed)
        
        autofolio = AutofolioPost(root_path, args)
        autofolio.main()
        
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        traceback.print_exc()
        return 1

if __name__ == "__main__":
    sys.exit(main())
