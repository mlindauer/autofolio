'''
Created on Dec 8, 2014

@author: Marius Lindauer
'''

import os
import logging
import arff # liac-arff
import sys
import yaml

from aslib.instance import Instance

class ASlibMetainfo(object):
    '''
        all meta information about an algorithm selection scenario
    '''
    
    
    def __init__(self):
        # listed in description.txt
        self.scenario = None # string
        self.performance_measure = [] #list of strings
        self.performance_type = [] # list of "runtime" or "solution_quality"
        self.maximize = [] # list of "true" or "false"
        self.algorithm_cutoff_time = None # float
        self.algorithm_cutoff_memory = None # integer
        self.features_cutoff_time = None # float
        self.features_cutoff_memory = None # integer
        self.features_deterministic = [] # list of strings
        self.features_stochastic = [] # list of strings
        self.algorithms = [] # list of strings
        self.algortihms_deterministics = [] # list of strings
        self.algorithms_stochastic = [] # list of strings
        self.feature_group_dict = {} # string -> [] of strings
        self.feature_steps = []

        # extracted in other files
        self.features = []
        self.ground_truths = {} # type -> [values]
        self.cv_given = True
        
        # command line options
        self.options = None 

class ASlibParser(object):
    '''
        Parsing of a ASlib Scenario
    '''


    def __init__(self, scenario):
        '''
        Constructor
        '''
        self.scenario = scenario
        self.metainfo = ASlibMetainfo()
        self.splits = {} # mapping fold -> inst
        
        self.instances = {}
        
        description_file = os.path.join(scenario, "description.txt")
        if os.path.isfile(description_file):
            self._read_description(description_file)
        else:
            logging.error("Have not found \"description.txt\" in %s" %(scenario))
            sys.exit(3)
        cv_file = os.path.join(scenario, "cv.arff")
        if os.path.isfile(cv_file):
            self._read_cv(cv_file)
        else:
            logging.error("Have not found \"cv.arff\" in %s" %(scenario))
            sys.exit(4)
            
        algo_file = os.path.join(scenario, "algorithm_runs.arff")
        if os.path.isfile(algo_file):
            self._read_algorithm_runs(algo_file)
        else:
            logging.error("Have not found \"algorithm_runs.arff\" in %s" %(scenario))
            
    def get_metainfo(self):
        ''' returns MetaInfo object'''
        return self.metainfo
    
    def get_splits(self):
        ''' returns mapping foldID -> [instances]'''
        return self.splits
    
    def get_instances(self):
        ''' returns mapping instance_name -> Instance()'''
        return self.instances
        
    def _read_description(self, file_):
        '''
            reads description file
            and saves all meta information
        ''' 
        logging.info("Read %s" %(file_))
        
        with open(file_, "r") as fh:
            description = yaml.load(fh)
            
        self.metainfo.scenario = description.get('scenario_id')
        self.metainfo.performance_measure = description.get('performance_measures')

        self.metainfo.performance_measure = description.get('performance_measures') if isinstance(description.get('performance_measures'), list) else \
                                                [description.get('performance_measures')] 

        maximize = description.get('maximize')
        self.metainfo.maximize = maximize if isinstance(maximize, list) else \
            [maximize]

        performance_type = description.get('performance_type')
        self.metainfo.performance_type  = performance_type if isinstance(performance_type, list) else \
            [performance_type]
            
        self.metainfo.algorithm_cutoff_time = description.get('algorithm_cutoff_time')
        self.metainfo.features_cutoff_memory = description.get('algorithm_cutoff_memory')
        self.metainfo.features_cutoff_time = description.get('features_cutoff_time')
        self.metainfo.features_cutoff_memory = description.get('features_cutoff_memory')
        self.metainfo.features_deterministic = description.get('features_deterministic')
        if self.metainfo.features_deterministic is None:
            self.metainfo.features_deterministic = set()
        self.metainfo.features_stochastic = description.get('features_stochastic')
        if self.metainfo.features_stochastic is None:
            self.metainfo.features_stochastic = set()
        self.metainfo.algortihms_deterministics = description.get('algorithms_deterministic')
        if self.metainfo.algortihms_deterministics is None:
            self.metainfo.algortihms_deterministics = set()
        self.metainfo.algorithms_stochastic = description.get('algorithms_stochastic')
        if self.metainfo.algorithms_stochastic is None:
            self.metainfo.algorithms_stochastic = set()
        self.metainfo.feature_group_dict = description.get('feature_steps')
        self.metainfo.feature_steps = description.get('default_steps')
        
        for step, d in self.metainfo.feature_group_dict.items():
            if  d.get("requires") and not isinstance(d["requires"], list):
                self.metainfo.feature_group_dict[step]["requires"] = [d["requires"]]

        self.metainfo.algorithms = list(
            set(self.metainfo.algorithms_stochastic).union(
                self.metainfo.algortihms_deterministics))
        
        self.metainfo.algorithms = list(set(self.metainfo.algorithms_stochastic).union(self.metainfo.algortihms_deterministics))
                  
        if not self.metainfo.scenario:
            logging.warn("Have not found SCENARIO_ID")
        if not self.metainfo.performance_measure:
            logging.warn("Have not found PERFORMANCE_MEASURE")
        if not self.metainfo.performance_type:
            logging.warn("Have not found PERFORMANCE_TYPE")
        if not self.metainfo.maximize:
            logging.warn("Have not found MAXIMIZE")
        if not self.metainfo.algorithm_cutoff_time:
            Printer.print_e("Have not found algorithm_cutoff_time")
        if not self.metainfo.algorithm_cutoff_memory:
            logging.warn("Have not found algorithm_cutoff_memory")
        if not self.metainfo.features_cutoff_time:
            logging.warn("Have not found features_cutoff_time")
            logging.warn("Assumption FEATURES_CUTOFF_TIME == ALGORITHM_CUTOFF_TIME ")
            self.metainfo.features_cutoff_time = self.metainfo.algorithm_cutoff_time
        if not self.metainfo.features_cutoff_memory:
            logging.warn("Have not found features_cutoff_memory")
        if not self.metainfo.features_deterministic:
            logging.warn("Have not found features_deterministic")
        if not self.metainfo.features_stochastic:
            logging.warn("Have not found features_stochastic")
        if not self.metainfo.algortihms_deterministics:
            logging.warn("Have not found algortihms_deterministics")
        if not self.metainfo.algorithms_stochastic:
            logging.warn("Have not found algorithms_stochastic")
        if not self.metainfo.feature_group_dict:
            logging.warn("Have not found any feature step")
           
        feature_intersec = set(self.metainfo.features_deterministic).intersection(self.metainfo.features_stochastic)
        if feature_intersec:
            logging.warn("Intersection of deterministic and stochastic features is not empty: %s" %(str(feature_intersec)))
        algo_intersec = set(self.metainfo.algortihms_deterministics).intersection(self.metainfo.algorithms_stochastic)
        if algo_intersec:
            logging.warn("Intersection of deterministic and stochastic algorithms is not empty: %s" %(str(algo_intersec)))
        
    def _read_algorithm_runs(self, file_):
        '''
            read performance file
            and saves information
            add Instance() in self.instances
            
            unsuccessful runs are replaced by algorithm_cutoff_time if performance_type is runtime
            
            EXPECTED HEADER:
            @RELATION ALGORITHM_RUNS_2013-SAT-Competition

            @ATTRIBUTE instance_id STRING
            @ATTRIBUTE repetition NUMERIC
            @ATTRIBUTE algorithm STRING
            @ATTRIBUTE PAR10 NUMERIC
            @ATTRIBUTE Number_of_satisfied_clauses NUMERIC
            @ATTRIBUTE runstatus {ok, timeout, memout, not_applicable, crash, other}
        '''
        logging.info("Read %s" %(file_))
        
        fp = open(file_,"rb")
        arff_dict = arff.load(fp)
        fp.close()
        
        if arff_dict["attributes"][0][0].upper() != "INSTANCE_ID":
            logging.error("instance_id as first attribute is missing in %s" %(file_))
        if arff_dict["attributes"][1][0].upper() != "REPETITION":
            logging.error("repetition as second attribute is missing in %s" %(file_))    
        if arff_dict["attributes"][2][0].upper() != "ALGORITHM":
            logging.error("algorithm as third attribute is missing in %s" %(file_))
            
        i = 0
        for performance_measure in self.metainfo.performance_measure:                       
            if arff_dict["attributes"][3 + i][0].upper() != performance_measure.upper():
                logging.error("\"%s\" as attribute is missing in %s" %(performance_measure, file_))
            i += 1
        
        if arff_dict["attributes"][3 + i][0].upper() != "RUNSTATUS":
            logging.error("runstatus as last attribute is missing in %s" %(file_))
               
        pairs_inst_rep_alg = []
        for data in arff_dict["data"]:
            inst_name = str(data[0])
            repetition = data[1]
            algorithm = str(data[2])
            perf_list = data[3:-1]
            status = data[-1]
            
            inst_ = self.instances.get(inst_name,Instance(inst_name))
            
            for p_measure, p_type, perf in zip(self.metainfo.performance_measure, self.metainfo.performance_type, perf_list):       
                if perf is None:
                    logging.error("The following performance data has missing values.\n"+ 
                                    "%s" % (",".join(map(str,data))))
                if p_type == "runtime" and status.upper() != "OK": # if broken run, replace with cutoff time
                    perf = self.metainfo.algorithm_cutoff_time + 1
                inst_._cost[p_measure] = inst_._cost.get(p_measure,{})
                perf_measure_dict = inst_._cost[p_measure]
                perf_measure_dict[algorithm] = perf_measure_dict.get(algorithm,[])
                perf_measure_dict[algorithm].append(max(float(perf),0.00001))
            
            inst_._status[algorithm] = status.upper()
            
            self.instances[inst_name] = inst_
            if (inst_name,repetition, algorithm) in pairs_inst_rep_alg:
                logging.warn("Pair (%s,%s,%s) is not unique in %s" %(inst_name, repetition, algorithm, file_))
            else:
                pairs_inst_rep_alg.append((inst_name,repetition, algorithm))

        
    def _read_cv(self, file_):
        '''
            read cross validation <file_>
            ATTENTION: Read only first repetition!
            
            @RELATION CV_2013 - SAT - Competition
            @ATTRIBUTE instance_id STRING
            @ATTRIBUTE repetition NUMERIC
            @ATTRIBUTE fold NUMERIC
        '''
        logging.info("Read %s" %(file_))
        
        fp = open(file_,"rb")
        arff_dict = arff.load(fp)
        fp.close()
        
        if arff_dict["attributes"][0][0] != "instance_id":
            logging.error("instance_id as first attribute is missing in %s" %(file_))
        if arff_dict["attributes"][1][0] != "repetition":
            logging.error("repetition as second attribute is missing in %s" %(file_))
        if arff_dict["attributes"][2][0] != "fold":
            logging.error("fold as third attribute is missing in %s" %(file_))
        
        for data in arff_dict["data"]:
            inst_name = data[0]
            fold = int(data[2])
            
            self.splits[fold] = self.splits.get(fold, [])
            self.splits[fold].append(inst_name)
            
