'''
Created on Feb 18, 2015

@author: Marius Lindauer
'''

import os
from subprocess import Popen

class AblationAnalysis(object):
    '''
      runs Ablation [C. Fawcett and H. Hoos 2015] to determine the important parameters between default and optimized configuration
    '''


    def __init__(self, root):
        '''
        Constructor
        '''
        self._PATH = os.path.join(root, "ablation", "ablationAnalysis-0.9.2", "ablationAnalysis")
        
    def generate_calls(self, scenario, configs):
        
        calls = []
        for rep, conf in enumerate(configs):
            rep += 1
            scen_file = "%s_f%d.scen" %(scenario, rep)
            
            self._write_ablation_option_file(scen_file, conf)
            
            cmd = "%s --optionFile %s.ablation --numRun %d > ablation_%d.log" %(self._PATH, scen_file, rep, rep)
            
            calls.append(cmd)

        return calls


    def _write_ablation_option_file(self, scen_file, target_conf):
        '''
           reads the original scenario SMAC file and modifies it for Ablation 
        '''
        with open(scen_file+".ablation", "w") as abl_fp:
            with open(scen_file) as fp:
                for line in fp:
                    if "wallclock-limit" in line:
                        continue
                    abl_fp.write(line)
            abl_fp.write("sourceConfiguration=DEFAULT\n")
            abl_fp.write("targetConfiguration=\"%s\"\n" %(target_conf))
            abl_fp.write("useRacing = false\n")
            
        
                
        
        