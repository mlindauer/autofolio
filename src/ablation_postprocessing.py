#!/usr/local/bin/python2.7
# encoding: utf-8
'''
autofolio -- algorithm configuration for algorithm selection - 

Post-processing script to evaluate ablation analysis

@author:     Marius Lindauer

@copyright:  2014 Marius Lindauer. All rights reserved.

@license:    GPLv3

@contact:    lindauer@cs.uni-freiburg.de    
'''

import sys
import os
import logging
import traceback
import random
import shutil
import arff # liac-arff
import json
import re

import numpy

import matplotlib
#matplotlib.use("Agg")
from pylab import plot, grid, show, xticks, subplots_adjust, savefig, ylabel

from argparse import ArgumentParser
from argparse import ArgumentDefaultsHelpFormatter
from argparse import SUPPRESS

matplotlib.use("pdf")

from selector.claspfolio import ClaspfolioSpecifics
from aslib.aslib_parser import ASlibParser

__all__ = []
__version__ = 0.1
__date__ = '2014-12-08'
__updated__ = '2015-01-26'

class AblationPost(object):
    
    def __init__(self, root_path, args):
        ''' Constructor '''
        self.root_path = root_path
        
        self.scenario = args.scenario
        self.name = os.path.basename(self.scenario.strip("/"))
        self.selector = args.selector
        self.cutoff = args.cutoff
        
        #Configuration
        self.reps = args.repetitions
        
        self.wrapper = args.wrapper
        if not os.path.isfile(self.wrapper):
            self.wrapper = os.path.join(root_path, self.wrapper)
            if not os.path.isfile(self.wrapper):
                logging.error("Have not found target algorithm wrapper (--wrapper): %s" %(args.wrapper))
        
        self.configurator = args.configurator
        
        self._OUTER_FOLDS = 10
        
    def main(self):
        ''' main method '''
        as_parser = ASlibParser(self.scenario)
        metainfo = as_parser.get_metainfo()
        instance_dict = as_parser.get_instances()
        rep_path_dict = self.read_ablation_log()
        logging.debug(rep_path_dict)
        self.evaluate_pathes(rep_path_dict, instance_dict)
        
        
    def read_ablation_log(self):
        ''' read ablation-run<id>.txt in <DIR> to get ablation paths'''
        
        logging.info("Reading Ablation Log-Files")
        DIR = "log"
        
        if not os.path.isdir(DIR):
            logging.error("Not found ablation log files in %s" %(DIR))
            sys.exit(1)
        
        log_pattern = re.compile("ablation-run(?P<id>\d+)\.txt")
        rep_path_dict =  {}
        for file_ in os.listdir(DIR):
            log_match = log_pattern.match(file_)
            if log_match:
                rep = int(log_match.groups("id")[0])
                path = []
                flipped = []
                with open(os.path.join(DIR,file_)) as fp:
                    for line in fp:
                        line = line.strip("\n")
                        if line.startswith("Source configuration:"):
                            path.append(line.lstrip("Source configuration:"))
                        if  line.startswith("Configuration"):
                            path.append(line.lstrip("Configuration :").strip("\""))
                        if line.startswith("Ablation Round"):
                            flipped.append(line.split("[")[1].split("]")[0])
                if len(path) == len(flipped)+1:
                    rep_path_dict[rep] = {"path":path, "flipped":flipped}
                else:
                    logging.error("Ablation log file incomplete (%s)" %(file_))
                
        return rep_path_dict
                            
    def evaluate_pathes(self, rep_path_dict, instance_dict):
        ''' assess the performance of the ablation pathes in the test data'''
        
        flip_score = {}
        flip_rank = {}
        for rep, path_flips in rep_path_dict.iteritems():
            path = path_flips["path"]
            path_perf = []
            # assess performance of each config in ablation path on test
            for step, conf in enumerate(path):
                cf = ClaspfolioSpecifics()
                conf_dict, _ = cf.evaluate(conf, self.scenario, rep, self.wrapper, self.cutoff, 
                                           out_conf="ablation_r%d_s%d.csv" %(rep, step), 
                                           out_def=None, default=False)
                for name, inst_ in instance_dict.iteritems():
                    if "OK" not in inst_._status.values():
                        try:
                            del conf_dict[name]
                        except:
                            pass
                par10_perf = sum(conf_dict.values())/len(conf_dict)
                path_perf.append(par10_perf)
                if step == 0:
                    logging.debug("Perf: %.2f" %(par10_perf))
                else:
                    logging.debug("Perf: %.2f by flipping: %s" %(par10_perf, path_flips["flipped"][step-1]))
            # normalize performances
            diff = path_perf[0] - path_perf[-1]
            
            for id_, flip in enumerate(path_flips["flipped"]):
                score = (path_perf[id_] - path_perf[id_+1]) / diff
                flip_score[flip] = flip_score.get(flip,[])
                flip_score[flip].append(score)
                flip_rank[flip] = flip_rank.get(flip,[])
                flip_rank[flip].append(id_)
            
            print(path_flips["flipped"])
            plot(path_perf)
            xticks(range(1,len(path_perf)+1), map(lambda x: x.replace("@1:","").replace("F:",""), path_flips["flipped"]), rotation=30, ha="right")
            grid(True)
            ylabel("PAR10")
            subplots_adjust(bottom=.25)
            savefig("ablation_path_%d.pdf"%(rep), format='pdf')
            show()
        
        
        reps = len(rep_path_dict)
        flip_stats = {}
        print(flip_score)
        for flip, scores in flip_score.iteritems():
            if len(scores) > reps:
                scores.extend([0]*(reps-len(scores)))
            flip_stats[flip] = (numpy.average(scores), numpy.std(scores), numpy.average(flip_rank[flip]), len(flip_rank[flip]))
          
        flip_stats = sorted(flip_stats.items(), key=lambda x: x[1][0], reverse=True)
            
        logging.info(">>> Results (mean +- std [mean rank, #]:")
        for flip, stats in flip_stats:
            logging.info("%s: %.2f +- %.2f [%.2f, %d]" %(flip,stats[0], stats[1], stats[2], stats[3]))
        
def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    #program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Marius Lindauer on %s.
  Copyright 2015. All rights reserved.

  Licensed under the GPLv3
  http://www.gnu.org/copyleft/gpl.html

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        
        root_path = os.path.join(os.path.split(sys.argv[0])[0], "..")
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=ArgumentDefaultsHelpFormatter)
        
        req_group = parser.add_argument_group('Required Options')
        req_group.add_argument("-s", '--scenario', required=True, help="aslib scenario")
        #parser.add_argument('--selector', default="claspfolio", choices=["claspfolio"], help="algorithm selector to configure")
        
        #test_options = parser.add_argument_group("Test Options")
        #test_options.add_argument("-p", "--permutations", type=int, default=100000, help="number of permutations in statistical test")
        #test_options.add_argument("-a", "--alpha", type=float, default=0.05, help="significance level (alpha) in statistical test")
        #test_options.add_argument("-f", "--fanova", action="store_true", default=False, help="apply fAnova to get parameter importance")
        #test_options.add_argument("-A", "--ablation", action="store_true", default=False, help=SUPPRESS) #help="apply Ablation to get parameter importance")
        #test_options.add_argument("-C", '--cluster', default="local", choices=["local", "meta","bwuni","orcinus"], help="cluster job submitting system where the *Ablation* runs are submitted to")
                
        conf_group = parser.add_argument_group('Configuration Options')
        conf_group.add_argument("-r", '--repetitions', type=int, default=1, help="number of configuration runs per split")
        conf_group.add_argument("-c", '--cutoff', default=3600, type=int, help="target algorithm selector runtime cutoff")
        conf_group.add_argument('--wrapper', default="wrappers/claspfolio/cf_wrapper.py", help="wrapper of algorithm selector.py")
        conf_group.add_argument('--selector', default="claspfolio", choices=["claspfolio"], help="selector to configure - has to compatible with wrapper")
        conf_group.add_argument('--configurator', default="SMAC", choices=["SMAC"], help="call of configurator (only SMAC is supported)")
        
        misc_group = parser.add_argument_group('MISC Options')
        misc_group.add_argument("-v", "--verbose", dest="verbose", default="INFO", choices=["INFO", "DEBUG","NOTEST"], help="set verbosity level [default: %(default)s]")
        misc_group.add_argument("-V", "--version", action='version', version=program_version_message)
        misc_group.add_argument("-S", '--seed', type=int, default=12345, help="random seed")
        
        # Process arguments
        args = parser.parse_args()
        logging.basicConfig(format="[%(levelname)s]: %(message)s", level=args.verbose)
        random.seed(args.seed)
        
        autofolio = AblationPost(root_path, args)
        autofolio.main()
        
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        traceback.print_exc()
        return 1

if __name__ == "__main__":
    sys.exit(main())
