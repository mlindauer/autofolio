'''
Created on Jan 17, 2015

@author: Marius Lindauer
'''

import os
from subprocess import Popen
from pyfanova.fanova import Fanova
import json
import logging

from shutil import move
from os import remove

#from fANOVA.state_merge import state_merge

class FANOVA(object):
    '''
        apply functional ANOVA (Hutter et al. 2012) to get the importance of parameters
        1. Collect target algorithm data over configuration repetitions of one fold
        2. Apply fANOVA
        3. Average parameter importance over folds
    '''

    def __init__(self, root_path):
        '''
        Constructor
        '''
        
        self.root_path = root_path 
        self._MERGE_TOOL = os.path.join(root_path,"configurators/smac-v2.08.00-master-731/util/state-merge")
        
    
    def get_importance(self, scenario, reps):
        '''
        '''
        # ~/git/autofolio/configurators/smac-v2.08.00-master-731/util/state-merge --directories ./smac-output/SAT12-ALL_f1/ --scenario-file SAT12-ALL_f1.scen --outdir merge-test_f1
        # increase cutoff
        
        param_marginals = {"approach_pairwise":{}}
        for rep in xrange(reps):
            merge_dir = "%s_f%d_merged" %(scenario, rep+1)
            
            if os.path.isdir(merge_dir):
                logging.info("Found merging directory: %s; Skip merging" %(merge_dir))
            else:
                cmd = "%s --directories ./smac-output/%s_f%d/ --scenario-file %s_f%d.scen --outdir %s" %(self._MERGE_TOOL,
                                                                                                                    scenario, rep+1,
                                                                                                                    scenario, rep+1,
                                                                                                                    merge_dir)
                p = Popen(cmd, shell=True)
                p.communicate()
                #===============================================================
                # base_dir = "smac-output/%s_f%d/" %(scenario,rep+1)
                # dir_list = [base_dir+"state-run%d" %(i) for i in xrange(1,13)]
                # state_merge(state_run_directory_list=dir_list, destination=merge_dir)
                #===============================================================
                
            p = Popen("sed '/TIMEOUT/d' %s/runs_and_results* -i" %(merge_dir), shell=True)
            p.communicate()
            p = Popen("sed '/SBS/d' %s/runs_and_results* -i" %(merge_dir), shell=True)
            p.communicate()
                                
            self.__replace_cutoff_time(merge_dir)
            
            logging.info("Running fAnova (fold: %d)" %(rep))
            f = Fanova(merge_dir)#, improvement_over="DEFAULT")
            
            param_names = f._config_space.get_parameter_names()
            num_params = len(param_names)
    
            main_marginal_performances = [f.get_marginal(i) for i in range(num_params)]
            for marginal, param_name in zip(main_marginal_performances, param_names):
                param_marginals[param_name] = param_marginals.get(param_name, [])
                param_marginals[param_name].append(marginal)
            
            pairwise_marginal_performances = [f.get_pairwise_marginal("@1:approach", p) for p in param_names]
            for marginal, param_name in zip(pairwise_marginal_performances, param_names):
                param_marginals["approach_pairwise"][param_name] = param_marginals["approach_pairwise"].get(param_name, [])
                param_marginals["approach_pairwise"][param_name].append(marginal)
            
            del f
        
        #logging.debug(json.dumps(param_marginals, indent=2))
        return param_marginals
        
    def __replace_cutoff_time(self, merge_dir):
        '''
             fAnova 0.1 has a bug for quality optimization; 
             we have to increase the runtime cutoff in the scenario file to prevent capping of our results
        '''
        
        new_scenario = os.path.join(merge_dir, "scenario_new.txt")
        old_scenario = os.path.join(merge_dir, "scenario.txt")
        with open(new_scenario, "w") as new_fp:
            with open(old_scenario, "r") as old_fp:
                for line in old_fp:
                    if line.startswith("cutoff_time"):
                        new_fp.write("cutoff_time=10000000000\n")
                    else:
                        new_fp.write(line)
        
        remove(old_scenario)
        move(new_scenario, old_scenario)
    
        
        
        