#!/usr/local/bin/python2.7
# encoding: utf-8
'''
autofolio -- algorithm configuration for algorithm selection

@author:     Marius Lindauer    

@copyright:  2014 Marius Lindauer. All rights reserved.

@license:    GPLv3

@contact:    lindauer@cs.uni-freiburg.de    
'''

import sys
import os
import logging
import traceback
import random
import shutil
import arff  # liac-arff

from argparse import ArgumentParser
from argparse import ArgumentDefaultsHelpFormatter

from aslib.aslib_parser import ASlibParser

from selector.claspfolio import ClaspfolioSpecifics
from selector.flexfolio import FlexfolioSpecifics
from configurator_interfaces.smac import SMAC
from job_systems.local import LocalExecutor
from job_systems.meta import MetaSubmitter
from job_systems.bwuni import BWUniSubmitter
from job_systems.orcinus import OrcinusSubmitter

__all__ = []
__version__ = 0.1
__date__ = '2014-12-08'
__updated__ = '2015-01-07'


class Autofolio(object):

    def __init__(self, root_path, args):
        ''' Constructor '''
        self.root_path = root_path

        self.scenario = args.scenario

        # Configuration
        self.budget = args.budget
        self.cutoff = args.cutoff
        self.reps = args.repetitions
        self.wrapper = args.wrapper
        if not os.path.isfile(self.wrapper):
            self.wrapper = os.path.join(root_path, self.wrapper)
            if not os.path.isfile(self.wrapper):
                logging.error(
                    "Have not found target algorithm wrapper (--wrapper): %s" % (args.wrapper))
        self.configurator = args.configurator

        # PCS
        self.selector = args.selector
        self.pcs = args.pcs
        self.add_features = args.add_features
        self.add_groups = args.add_feature_groups
        self.add_algorithms = args.add_algorithms

        # JOB
        self.clustersystem = args.cluster
        if self.clustersystem == "orcinus":
            self.MEMLIMIT = 800
        else:
            self.MEMLIMIT = 2000

        # MISC
        self.tmp = args.tmp
        self.runsolver = args.runsolver

        self.basename_scenario = None

        self._INNER_FOLDS = 10

    def main(self):
        ''' main method '''
        as_parser = ASlibParser(self.scenario)
        metainfo = as_parser.get_metainfo()
        splits = as_parser.get_splits()
        scenarios = self.create_inner_scenarios(splits)
        pcss = self.add_parameters_to_pcs(scenarios, metainfo)
        calls = self.write_conf_scenarios(pcss)
        self.submit_job(calls)

    def create_inner_scenarios(self, splits):
        '''
            based on given CV splits, creates further inner CV splits
            copy remaining scenarios
            Args: 
                splits: outer cv splits (fold -> [instances])
        '''
        logging.debug("Write new scenarios")
        attributes = [("instance_id", "STRING"),
                      ("repetition", "NUMERIC"),
                      ("fold", "NUMERIC")
                      ]

        basename_scenario = os.path.basename(self.scenario.strip("/"))
        self.basename_scenario = basename_scenario

        scenarios = []
        all_insts = set()
        for s in splits.values():
            for i in s:
                all_insts.add(i)

        for fold, insts in splits.iteritems():
            # use all instances except fold
            train_insts = list(all_insts.difference(insts))
            random.shuffle(train_insts)
            data = []
            scen_fold_name = basename_scenario + "_f%d" % (fold)
            scenarios.append(scen_fold_name)

            # ATTENTION: Deletes old files!
            if os.path.isdir(scen_fold_name):
                logging.warn("Overwrite old files")
                shutil.rmtree(scen_fold_name)
            shutil.copytree(self.scenario, scen_fold_name)
            for fold_id, fold_insts in enumerate([train_insts[i::self._INNER_FOLDS] for i in xrange(self._INNER_FOLDS)]):
                for i in fold_insts:
                    data.append([i, 1, fold_id + 1])
            all_data = {"attributes": attributes,
                        "data": data,
                        "relation": scen_fold_name
                        }

            # write new cv.arff
            with open(os.path.join(scen_fold_name, "cv.arff"), "w") as fp:
                arff.dump(all_data, fp)

            # keep only used instances
            self._remove_insts(
                train_insts, scen_fold_name, "algorithm_runs.arff")
            self._remove_insts(
                train_insts, scen_fold_name, "feature_costs.arff")
            self._remove_insts(
                train_insts, scen_fold_name, "feature_runstatus.arff")
            self._remove_insts(
                train_insts, scen_fold_name, "feature_values.arff")
            self._remove_insts(
                train_insts, scen_fold_name, "ground_truth.arff")

        return scenarios

    def _remove_insts(self, insts_2_keep, scen_fold_name, file_name):
        '''
            remove instances from scenario files such that it consistent with cv.arff
        '''
        if os.path.isfile(os.path.join(scen_fold_name, file_name)):
            with open(os.path.join(scen_fold_name, file_name), "r") as fp:
                data_dict = arff.load(fp)
            with open(os.path.join(scen_fold_name, file_name), "w") as fp:
                new_data = []
                for row in data_dict["data"]:
                    if row[0] in insts_2_keep:
                        new_data.append(
                            map(lambda x: x if x != None else "?", row))
                data_dict["data"] = new_data
                arff.dump(data_dict, fp)

    def add_parameters_to_pcs(self, scenarios, metainfo):
        '''
            add parameters to pcs
        '''
        logging.debug("Write extended pcs file")
        pcss = []
        if self.selector == "claspfolio":
            cf_specs = ClaspfolioSpecifics()
            for scen in scenarios:
                pcs = cf_specs.extend_pcs(self.pcs, scen, metainfo,
                                          self.add_features, self.add_groups, self.add_algorithms,
                                          self.MEMLIMIT)
                pcss.append(pcs)
        if self.selector == "flexfolio":
            cf_specs = FlexfolioSpecifics()
            for scen in scenarios:
                pcs = cf_specs.extend_pcs(self.pcs, scen, metainfo,
                                          self.add_features, self.add_groups, self.add_algorithms,
                                          self.MEMLIMIT)
                pcss.append(pcs)
        return pcss

    def write_conf_scenarios(self, pcss):
        '''
            write the configuration scenarios
        '''
        logging.debug("Write SMAC scenarios")
        if self.configurator == "SMAC":
            smac = SMAC(self.root_path, self.reps, self.budget,
                        self.cutoff, self.wrapper, self.tmp, self.runsolver)
            calls = smac.get_ac_calls(pcss, self._INNER_FOLDS)

        for c in calls:
            logging.debug(c)
        return calls

    def submit_job(self, calls):
        '''
            based on <clustersystem> submit jobs (or execute it locally) 
        '''
        if self.clustersystem == "local":
            runner = LocalExecutor(cores=4)
            runner.submit_jobs(calls)
        elif self.clustersystem == "meta":
            submitter = MetaSubmitter(self.root_path)
            submitter.submit_jobs(calls, self.basename_scenario)
        elif self.clustersystem == "bwuni":
            submitter = BWUniSubmitter(self.budget)
            submitter.submit_jobs(calls, self.basename_scenario)
        elif self.clustersystem == "orcinus":
            submitter = OrcinusSubmitter(self.budget)
            submitter.submit_jobs(calls, self.basename_scenario)


def main(argv=None):  # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    #program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (
        program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Marius Lindauer on %s.
  Copyright 2015. All rights reserved.

  Licensed under the GPLv3
  http://www.gnu.org/copyleft/gpl.html

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:

        root_path = os.path.join(os.path.split(sys.argv[0])[0], "..")
        # Setup argument parser
        parser = ArgumentParser(
            description=program_license, formatter_class=ArgumentDefaultsHelpFormatter)

        req_group = parser.add_argument_group('Required Options')
        req_group.add_argument(
            "-s", '--scenario', required=True, help="aslib scenario")
        #parser.add_argument('--selector', default="claspfolio", choices=["claspfolio"], help="algorithm selector to configure")

        conf_group = parser.add_argument_group('Configuration Options')
        conf_group.add_argument(
            "-b", '--budget', default=7200, type=int, help="configuration budget")
        conf_group.add_argument(
            "-c", '--cutoff', default=3600, type=int, help="target algorithm selector runtime cutoff")
        conf_group.add_argument(
            "-r", '--repetitions', type=int, default=1, help="number of configuration runs per split")
        conf_group.add_argument(
            '--wrapper', default="wrappers/flexfolio/ff_wrapper.py", help="wrapper of algorithm selector.py")
        conf_group.add_argument('--selector', default="flexfolio", choices=[
                                "claspfolio", "flexfolio"], help="selector to configure - has to compatible with wrapper")
        conf_group.add_argument('--configurator', default="SMAC",
                                choices=["SMAC"], help="call of configurator (only SMAC is supported)")
        conf_group.add_argument("-C", '--cluster', default="local", choices=[
                                "local", "meta", "bwuni", "orcinus"], help="cluster job submitting system where the configuration runs are submitted to")

        pcs_group = parser.add_argument_group('PCS Options')
        pcs_group.add_argument('--pcs', default=os.path.join(root_path,
                                                             "pcs/flexfolio-df-normal.txt"), help="psc file of selector")
        pcs_group.add_argument(
            '-f', '--add_features', default=False, action="store_true", help="add features to pcs")
        pcs_group.add_argument('-g', '--add_feature_groups', default=False,
                               action="store_true", help="add features groups to pcs")
        pcs_group.add_argument(
            '-a', '--add_algorithms', default=False, action="store_true", help="add algorithms to pcs")

        misc_group = parser.add_argument_group('MISC Options')
        misc_group.add_argument("-v", "--verbose", dest="verbose", default="INFO", choices=[
                                "INFO", "DEBUG", "NOTEST"], help="set verbosity level [default: %(default)s]")
        misc_group.add_argument(
            "-V", "--version", action='version', version=program_version_message)
        misc_group.add_argument(
            "-S", '--seed', type=int, default=12345, help="random seed")
        misc_group.add_argument(
            "-t", '--tmp', default="/tmp/", help="directory for temporary files")
        misc_group.add_argument("-R", '--runsolver', default=os.path.join(
            root_path, "runsolver", "runsolver"), help="runsolver to limit the runtime of target algorithm")

        # Process arguments
        args = parser.parse_args()
        logging.basicConfig(
            format="[%(levelname)s]: %(message)s", level=args.verbose)
        random.seed(args.seed)

        autofolio = Autofolio(root_path, args)
        autofolio.main()

    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        traceback.print_exc()
        return 1

if __name__ == "__main__":
    sys.exit(main())
