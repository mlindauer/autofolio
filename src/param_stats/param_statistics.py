'''
Created on Jan 7, 2015

@author: Marius Lindauer
'''

class ParamStatistics(object):
    '''
       given a list of configurations,
       generate some statistics about the similarity of the configurations
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.freqs = {} # param -> value -> frequencys
        
    def get_stats(self, configs):
        '''
            main method of class (see class docu)
        '''
        self._frequencies(configs)
        return self.freqs
        
    def _frequencies(self, configs):
        ''' count frequency of parameters with their values'''

        for conf in configs:
            conf = conf.split(" ")
            for param, value in zip(conf[::2], conf[1::2]):
                self.freqs[param] = self.freqs.get(param,{})
                self.freqs[param][value] = self.freqs[param].get(value, 0) + 1
        
        