'''
Created on Feb 07, 2015

@author: Marius Lindauer
'''

import os
import sys
import json
import logging
import numpy

class TablefAnova(object):
    
    def __init__(self):
        ''' Constructor '''
        self._data = {} # scenario -> pcs type -> results
        
        self._scen_macros = {"ASP-POTASSCO" : "\\aspCoseal",
                             "CSP-2010" : "\\cspCoseal",
                             "MAXSAT12-PMS" : "\maxsatCoseal",
                             "PREMARSHALLING-ASTAR-2013" : "\premarCoseal",
                             "PROTEUS-2014" : "\proteusCoseal",
                             "QBF-2011" : "\qbfCoseal",
                             "SAT12-ALL" : "\satallTwelveCoseal",
                             "SAT12-HAND" : "\sathandTwelveCoseal",
                             "SAT12-INDU" : "\satinduTwelveCoseal",
                             "SAT12-RAND" : "\satrandTwelveCoseal",
                             "SAT11-HAND" : "\sathandElevenCoseal",
                             "SAT11-INDU" : "\sathandElevenCoseal",
                             "SAT11-RAND" : "\satrandElevenCoseal"
                             }
        
        self._N = 10
        
    def main(self):
        
        try:
            with open(sys.argv[1], "r") as fp:
                param_effect = json.load(fp)["fanova"]
        except:
            logging.error("Could not read json input file: %s" %(sys.argv[1]))
          
        approach_param_effect = param_effect["approach_pairwise"]
        del approach_param_effect["@1:approach"]
        
        try:
            del param_effect["approach_pairwise"]
        except:
            pass
            
        self.print_table(param_effect)
        self.print_table(approach_param_effect)

    def print_table(self, param_effect):
        param_stat = {}
        for param, effects in param_effect.iteritems():
#            print(param, effects)
            effects = numpy.array(effects)
            avg = numpy.average(effects)
            std = numpy.std(effects)
            param_stat[param] = {"avg": avg, "std": std}
            
        sorted_params = sorted(param_stat.items(), key=lambda x: x[1]["avg"], reverse=True)
        
        print("\\begin{table}")
        print("\centering")
        print("\\begin{tabular}[t]{lr}")
        print("\\toprule[1pt]")
        print("Parameter & Main Effect\\\\")
        print("\\midrule[1pt]")
        for sparam in sorted_params[:self._N]:
            print("%s & $%.2f\\%% \\pm %.2f$\\\\" %(sparam[0].strip("@1:"), sparam[1]["avg"], sparam[1]["std"]))
        print("\\bottomrule[1pt]")
        print("\\end{tabular}")
        print("\\end{table}")

if __name__ == '__main__':
    cat = TablefAnova()
    cat.main()
