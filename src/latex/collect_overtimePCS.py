'''
Created on Jan 19, 2015

@author: Marius Lindauer
'''

import os
import sys
import json
import logging

import numpy
import matplotlib.pyplot as plt

class CollectOvertimePCS(object):
    
    def __init__(self):
        ''' Constructor '''
        
        logging.basicConfig(format="[%(levelname)s]: %(message)s", level="INFO")
        
        self._data = {} # scenario -> pcs type -> results
        
        self._scen_macros = {"ASP-POTASSCO" : "\\aspCoseal",
                             "CSP-2010" : "\\cspCoseal",
                             "MAXSAT12-PMS" : "\maxsatCoseal",
                             "PREMARSHALLING-ASTAR-2013" : "\premarCoseal",
                             "PROTEUS-2014" : "\proteusCoseal",
                             "QBF-2011" : "\qbfCoseal",
                             "SAT12-ALL" : "\satallTwelveCoseal",
                             "SAT12-HAND" : "\sathandTwelveCoseal",
                             "SAT12-INDU" : "\satinduTwelveCoseal",
                             "SAT12-RAND" : "\satrandTwelveCoseal",
                             "SAT11-HAND" : "\sathandElevenCoseal",
                             "SAT11-INDU" : "\sathandElevenCoseal",
                             "SAT11-RAND" : "\satrandElevenCoseal"
                             }
        
        self._scen_solvable = {"ASP-POTASSCO" : 1212,
                             "CSP-2010" : 1771,
                             "MAXSAT12-PMS" : 747,
                             "PREMARSHALLING-ASTAR-2013" : 527,
                             "PROTEUS-2014" : 3593,
                             "QBF-2011" : 1054,
                             "SAT12-ALL" : 1594,
                             "SAT12-HAND" : 538,
                             "SAT12-INDU" : 958,
                             "SAT12-RAND" : 1040,
                             "SAT11-HAND" : 223,
                             "SAT11-INDU" : 253,
                             "SAT11-RAND" : 492
                             }
        
        self._type_macro = {"voting": "Autofolio_vote",
                            "fgroups": "Autofolio",
                            "full": "Autofolio_ext"}
        
    def main(self):
        
        # collect data
        for dir_ in os.listdir("."):
            if os.path.isdir(dir_):
                type_ = dir_
                for scen_dir in os.listdir(dir_):
                    scen_dir = os.path.join(dir_, scen_dir)
                    if os.path.isdir(scen_dir):
                        data_file = os.path.join(scen_dir, "results.json")
                        if not os.path.isfile(data_file):
                            logging.warn(data_file)
                            continue
                        with open(data_file) as fp:
                            results = json.load(fp)
                            self._data[results["name"]] = self._data.get(results["name"], {})
                            self._data[results["name"]][type_] = results
        
        for scen, data in self._data.iteritems():
            #logging.info(scen)
            _, self.ax = plt.subplots()
            plt.ylabel("PAR10")
            plt.xlabel("Time (s)")
            plt.xscale("log",basex=2)
            #plt.yscale("log",basex=2)
            
            font = {'size'   : 18}
            plt.rc('font', **font)
            
            self._max = 0
            self._min = sys.maxint
            for type_, dict_ in data.iteritems():
                times = numpy.array(dict_["otplot"]["times"])
                medians = numpy.array(dict_["otplot"]["medians"])/(self._scen_solvable[scen]*0.09)
                q25 = numpy.array(dict_["otplot"]["q25"])/(self._scen_solvable[scen]*0.09)
                q75 = numpy.array(dict_["otplot"]["q75"])/(self._scen_solvable[scen]*0.09)
                self.plot(times, medians, q25, q75, self._type_macro[type_])
                
            plt.ylim(ymin=self._min*0.9,ymax=self._max*1.1)
            logging.debug("min: %.2f max:%.2f" %(self._min, self._max))
            plt.legend(loc=3)
            plt.tight_layout()
            plt.xlim(xmin=512,xmax=172800)
            out_ = "%s_overtime_pcs.pdf" %(scen)
            plt.savefig(out_, format="pdf")
            
            print("\\begin{figure}")
            print("\\includegraphics[width=\\textwidth]{images/pcs_perf_time/%s}" %(out_))
            print("\\caption{Performance over time for different configuration spaces on %s. Quantiles over outer CV folds.}" %(self._scen_macros[scen]))
            print("\\end{figure}")
            
            
    def plot(self, times, medians, q25, q75, type_):
        times = numpy.array(times)
        graph = self.ax.step(times, medians, label=type_)
        self.ax.fill_between(times, q25, q75, alpha=0.3, edgecolor='', facecolor=graph[0].get_color()) #, edgecolor='k')
      
        for idx, t in enumerate(times):
            if t > 1024:
                try:
                    self._max = max(q75[idx-1], self._max)
                except IndexError:
                    pass
                break
        self._min = min(q25[-1], self._min)
      
if __name__ == '__main__':
    cat = CollectOvertimePCS()
    cat.main()
