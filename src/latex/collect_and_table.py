'''
Created on Jan 19, 2015

@author: Marius Lindauer
'''

import os
import json
import logging

class CollectAndTrain(object):
    
    def __init__(self):
        ''' Constructor '''
        self._data = {} # scenario -> pcs type -> results
        
        self._scen_macros = {"ASP-POTASSCO" : "\\aspCoseal",
                             "CSP-2010" : "\\cspCoseal",
                             "MAXSAT12-PMS" : "\maxsatCoseal",
                             "PREMARSHALLING-ASTAR-2013" : "\premarCoseal",
                             "PROTEUS-2014" : "\proteusCoseal",
                             "QBF-2011" : "\qbfCoseal",
                             "SAT12-ALL" : "\satallTwelveCoseal",
                             "SAT12-HAND" : "\sathandTwelveCoseal",
                             "SAT12-INDU" : "\satinduTwelveCoseal",
                             "SAT12-RAND" : "\satrandTwelveCoseal",
                             "SAT11-HAND" : "\sathandElevenCoseal",
                             "SAT11-INDU" : "\sathandElevenCoseal",
                             "SAT11-RAND" : "\satrandElevenCoseal"
                             }
        
    def main(self):
        
        # collect data
        for dir_ in os.listdir("."):
            if os.path.isdir(dir_):
                type_ = dir_
                for scen_dir in os.listdir(dir_):
                    scen_dir = os.path.join(dir_, scen_dir)
                    if os.path.isdir(scen_dir):
                        data_file = os.path.join(scen_dir, "results.json")
                        if not os.path.isfile(data_file):
                            logging.warn(data_file)
                            continue
                        with open(data_file) as fp:
                            results = json.load(fp)
                            self._data[results["name"]] = self._data.get(results["name"], {})
                            self._data[results["name"]][type_] = results
        
        # print table
        
        print("\\begin{table}")
        print("\centering")
        print("\\begin{tabular}[t]{l cccccccc}")
        print("\\hline")
        print("\\hline")
        print("Scenario & \multicolumn{2}{c}{Default} & \multicolumn{2}{c}{Voting} & \multicolumn{2}{c}{MultiAS} & \multicolumn{2}{c}{Full}\\\\")
        print(" & PAR10 & $\#$TOs & PAR10 & $\#$TOs & PAR10 & $\#$TOs & PAR10 & $\#$TOs\\\\")
        print("\\hline")
        for scen in sorted(self._data.keys()):
            data = self._data[scen]
            string = ["%s" %(self._scen_macros[scen])]
            best = min(data["voting"]["default"]["par10"],
                       data["voting"]["configured"]["par10"],
                       data["fgroups"]["configured"]["par10"],
                       data["full"]["configured"]["par10"])
            best_to = min(data["voting"]["default"]["tos"],
                       data["voting"]["configured"]["tos"],
                       data["fgroups"]["configured"]["tos"],
                       data["full"]["configured"]["tos"])
            string.append(self.format_par10(data["voting"]["default"]["par10"], best, 0.5, data["voting"]["default"]["par10"]))
            string.append(self.format_to(data["voting"]["default"]["tos"], best_to))
            string.append(self.format_par10(data["voting"]["configured"]["par10"], best, data["voting"]["pvalue"], data["voting"]["default"]["par10"]))
            string.append(self.format_to(data["voting"]["configured"]["tos"], best_to))
            string.append(self.format_par10(data["fgroups"]["configured"]["par10"], best, data["fgroups"]["pvalue"], data["voting"]["default"]["par10"]))
            string.append(self.format_to(data["fgroups"]["configured"]["tos"], best_to))
            string.append(self.format_par10(data["full"]["configured"]["par10"], best, data["full"]["pvalue"], data["voting"]["default"]["par10"]))
            string.append(self.format_to(data["full"]["configured"]["tos"], best_to))
            print(" & ".join(string)+"\\\\")
        print("\\hline")
        print("\\hline")
        print("\end{tabular}")
        print('''\\caption{Comparing different configuration spaces of \\autofolio{}. 
\emph{Voting} considers only the pairwise voting scheme of \satzillaY{11}; 
\emph{MultiAS} considers all algorithm selection approaches; 
\emph{Full} extends \emph{MultiAS} by adding each feature and algorithm as binary parameters in the configuration space.
The best performance is marked with bold face.
$*$ or $\dagger$ indicate that the performance is significantly better than the default configuration of \\claspfolioTwo{} 
with a significance level $\\alpha=0.05$ or $\\alpha=0.1$, respectively (according to a permutation test with $100\ 000$ permutations). 
}''')
        print("\label{tab:perf}")
        print("\end{table}")
        
    def format_par10(self, value, best, pvalue, default):
        if value == best:
            str_ = "$\mathbf{%.1f}" %(value)
        else:
            str_ = "$%.1f" %(value)
        if value < default and pvalue <= 0.05:
            str_ += "^{*}"
        elif value < default and pvalue <= 0.1:
            str_ += "^{\dagger}"
        return str_+"$"
    
    def format_to(self, value, best):
        if value == best:
            str_ = "$\mathbf{%d}$" %(value)
        else:
            str_ = "$%d$" %(value)
        return str_

if __name__ == '__main__':
    cat = CollectAndTrain()
    cat.main()
