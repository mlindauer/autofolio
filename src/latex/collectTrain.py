'''
Created on Jan 19, 2015

@author: Marius Lindauer
'''

import os
import json
import logging

class CollectTrain(object):
    
    def __init__(self):
        ''' Constructor '''
        self._data = {} # scenario -> pcs type -> results
        
        self._scen_macros = {"ASP-POTASSCO" : "\\aspCoseal",
                             "CSP-2010" : "\\cspCoseal",
                             "MAXSAT12-PMS" : "\maxsatCoseal",
                             "PREMARSHALLING-ASTAR-2013" : "\premarCoseal",
                             "PROTEUS-2014" : "\proteusCoseal",
                             "QBF-2011" : "\qbfCoseal",
                             "SAT12-ALL" : "\satallTwelveCoseal",
                             "SAT12-HAND" : "\sathandTwelveCoseal",
                             "SAT12-INDU" : "\satinduTwelveCoseal",
                             "SAT12-RAND" : "\satrandTwelveCoseal",
                             "SAT11-HAND" : "\sathandElevenCoseal",
                             "SAT11-INDU" : "\sathandElevenCoseal",
                             "SAT11-RAND" : "\satrandElevenCoseal"
                             }
        
    def main(self):
        
        # collect data
        for dir_ in os.listdir("."):
            if os.path.isdir(dir_):
                type_ = dir_
                for scen_dir in os.listdir(dir_):
                    scen_dir = os.path.join(dir_, scen_dir)
                    if os.path.isdir(scen_dir):
                        data_file = os.path.join(scen_dir, "results.json")
                        if not os.path.isfile(data_file):
                            logging.warn(data_file)
                            continue
                        with open(data_file) as fp:
                            results = json.load(fp)
                            self._data[results["name"]] = self._data.get(results["name"], {})
                            self._data[results["name"]][type_] = results
        
        # print table
        
        print("\\begin{table}")
        print("\centering")
        print("\\begin{tabular}[t]{l ccc}")
        print("\\hline")
        print("\\hline")
        print("Scenario & Voting & MultiAS & Full\\\\")
        print("\\hline")
        for scen in sorted(self._data.keys()):
            data = self._data[scen]
            string = ["%s" %(self._scen_macros[scen])]
            best = min(data["voting"]["training"], data["fgroups"]["training"], data["full"]["training"])
            string.append(self.format(data["voting"]["training"], best))
            string.append(self.format(data["fgroups"]["training"], best))
            string.append(self.format(data["full"]["training"], best))
            print(" & ".join(string)+"\\\\")
        print("\\hline")
        print("\\hline")
        print("\end{tabular}")
        print('''\\caption{Comparing different configuration spaces of \\autofolio{} on PAR10 training performance. 
\emph{Voting} considers only the pairwise voting scheme of \satzillaY{11}; 
\emph{MultiAS} considers all algorithm selection approaches; 
\emph{Full} extends \emph{MultiAS} by adding each feature and algorithm as binary parameters in the configuration space.
The best performance is marked with bold face.}''')
        print("\label{tab:train}")
        print("\end{table}")
        
    def format(self, value, best):
        if value == best:
            str_ = "$\mathbf{%.1f}$" %(value)
        else:
            str_ = "$%.1f$" %(value)
        return str_

if __name__ == '__main__':
    cat = CollectTrain()
    cat.main()
