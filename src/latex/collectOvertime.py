'''
Created on Jan 23, 2015

@author: Marius Lindauer
'''

import os

class CollectOverTimePlots(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        
    def main(self):
        for root, dirs, files in os.walk("."):
            for f in files:
                if f.endswith("_overtime.pdf"):
                    print('''\\begin{figure}
\\includegraphics[width=\\textwidth]{images/%s}
\\caption{Overtime plot %s}
\\end{figure}''' %(os.path.join(root,f), root))
                    
        
if __name__ == '__main__':
    cat = CollectOverTimePlots()
    cat.main()