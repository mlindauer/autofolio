'''
Created on Jan 23, 2015

@author: Marius Lindauer
'''

import os

class CollectScatterPlots(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        
    def main(self):
        for root, dirs, files in os.walk("."):
            for f in files:
                if f.endswith(".pdf") and not f.endswith("_overtime.pdf"):
                    print('''\\begin{figure}
\\includegraphics[width=\\textwidth]{images/%s}
\\caption{Scatter plot %s}
\\end{figure}''' %(os.path.join(root,f), root))
                    
        
if __name__ == '__main__':
    cat = CollectScatterPlots()
    cat.main()