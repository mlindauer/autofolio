'''
Created on Jan 22, 2015

@author: Marius Lindauer
'''

import sys
import re
from subprocess import Popen

class VisualizePCS(object):
    '''
        visualize parameters and its conditionals via dot
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.FLOAT_REGEX = re.compile("^[ ]*(?P<name>[^ ]+)[ ]*\[(?P<range_start>[0-9]+(\.[0-9]+)?)[ ]*,[ ]*(?P<range_end>[0-9]+(\.[0-9]+)?)\][ ]*\[(?P<default>[^#]*)\](?P<misc>.*)$")
        self.CAT_REGEX = re.compile("^[ ]*(?P<name>[^ ]+)[ ]*{(?P<values>.+)}[ ]*\[(?P<default>[^#]*)\](?P<misc>.*)$")
        self.COND_REGEX = re.compile("^[ ]*(?P<cond>[^ ]+)[ ]*\|[ ]*(?P<head>[^ ]+)[ ]*in[ ]*{(?P<values>.+)}(?P<misc>.*)$")
        
        self.params = []
        self.arcs = []
        
    def main(self, pcs_file):
        self.read_pcs(pcs_file)
        self.write_dot()
        
    def read_pcs(self, pcs_file):
        with open(pcs_file, "r") as fp: 
            for line in fp:
                line = line.strip("\n").strip(" ")
                
                #remove comments
                if line.find("#") > -1:
                    line = line[:line.find("#")] # remove comments
                
                # skip empty lines
                if line  == "":
                    continue

                # categorial parameter
                cat_match = self.CAT_REGEX.match(line)
                float_match = self.FLOAT_REGEX.match(line)
                cond_match = self.COND_REGEX.match(line)
                
                if cat_match:
                    name = cat_match.group("name")
                    self.params.append(name)
                elif float_match:
                    name = float_match.group("name")
                    self.params.append(name)
                elif cond_match:
                    head = cond_match.group("head")
                    cond = cond_match.group("cond")
                    self.arcs.append((cond,head))
    
    def write_dot(self):
        with open("pcs.dot", "w") as fp:
            fp.write("digraph G {\n")
            used = set()
            for arc in self.arcs:
                fp.write("\"%s\" -> \"%s\";\n" %(arc[0], arc[1]))
                used.add(arc[0])
                used.add(arc[1])
            not_used = set(self.params).difference(used)
            for node in not_used:
                fp.write("\"%s\";\n" %(node))
            fp.write("}\n")
        
        
        
if __name__ == '__main__':
    vis = VisualizePCS()
    vis.main(sys.argv[1])