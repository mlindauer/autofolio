'''
Created on Dec 15, 2014

@author: lindauer
'''
import itertools
import numpy
import matplotlib
from matplotlib.pyplot import tight_layout, figure, subplots_adjust, subplot, savefig, show

class ScatterPlotter(object):
    '''
       generates a scatter plot based on two given vectors using matplotlib
    '''


    def __init__(self):
        '''
        Constructor
        '''
        
    def plot_scatter_plot(self, x_data, y_data, labels, title="", save="", debug=False,
                          min_val=0.01, max_val=300, grey_factor=1, linefactors=None):
        x_data = numpy.array(x_data)
        y_data = numpy.array(y_data)
        regular_marker = '+'
        timeout_marker = '.'
        grey_marker = 'x'
        c_angle_bisector = "#e41a1c"  # Red
        c_good_points = "#999999"     # Grey
        c_other_points = "k"
    
        st_ref = "--"
    
        # Colors
        ref_colors = itertools.cycle([  # "#e41a1c",    # Red
                                     "#377eb8",    # Blue
                                     "#4daf4a",    # Green
                                     "#984ea3",    # Purple
                                     "#ff7f00",    # Orange
                                     "#ffff33",    # Yellow
                                     "#a65628",    # Brown
                                     "#f781bf",    # Pink
                                     # "#999999",    # Grey
                                     ])
    
        # Set up figure
        ratio = 1
        gs = matplotlib.gridspec.GridSpec(ratio, 1)
        fig = figure(1, dpi=100)
        fig.suptitle(title, fontsize=16)
        ax1 = subplot(gs[0:ratio, :], aspect='equal')
        ax1.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
        
        ax1.set_autoscale_on(False)
        ax1.set_xlim([min_val, max_val*2])
        ax1.set_ylim([min_val, max_val*2])
    
        # Plot angle bisector and reference_lines
        out_up = max_val
        out_lo = min_val
    
        ax1.plot([out_lo, out_up], [out_lo, out_up], c=c_angle_bisector)
    
        if linefactors is not None:
            for f in linefactors:
                c = ref_colors.next()
                # Lower reference lines
                ax1.plot([f*out_lo, out_up], [out_lo, (1.0/f)*out_up], c=c, linestyle=st_ref)
                # Upper reference lines
                ax1.plot([out_lo, (1.0/f)*out_up], [f*out_lo, out_up], c=c, linestyle=st_ref)
    
                offset = 1.1
                if int(f) == f:
                    lf_str = "%dx" % f
                else:
                    lf_str = "%2.1fx" % f
                ax1.text((1.0/f)*out_up, out_up*offset, lf_str, color=c, fontsize=10)
                ax1.text(out_up*offset, (1.0/f)*out_up, lf_str, color=c, fontsize=10)
    
    
        # Scatter
        grey_idx = list()
        timeout_x = list()
        timeout_y = list()
        timeout_both = list()
        rest_idx = list()
        for idx_x, x in enumerate(x_data):
            if x >= max_val > y_data[idx_x]:
                # timeout of x algo
                timeout_x.append(idx_x)
            elif y_data[idx_x] >= max_val > x:
                # timeout of y algo
                timeout_y.append(idx_x)
            elif y_data[idx_x] >= max_val and x >= max_val:
                # timeout of both algos
                timeout_both.append(idx_x)
            elif y_data[idx_x] < grey_factor*x and x < grey_factor*y_data[idx_x]:
                grey_idx.append(idx_x)
            else:
                rest_idx.append(idx_x)
    
        # Regular points
        ax1.scatter(x_data[grey_idx], y_data[grey_idx], marker=grey_marker, c=c_good_points)
        ax1.scatter(x_data[rest_idx], y_data[rest_idx], marker=regular_marker, c=c_other_points)
    
        # Timeout points
        timeout_val = 1.0* max_val # int(np.log10(10*max_val)) 
        ax1.scatter([timeout_val]*len(timeout_x), y_data[timeout_x], marker=timeout_marker, c=c_other_points)
        
        ax1.scatter(x_data[timeout_y], [timeout_val]*len(timeout_y), marker=timeout_marker, c=c_other_points)
        if timeout_both:
            ax1.scatter([timeout_val], [timeout_val], marker=timeout_marker, c=c_other_points)
        # Timeout line
        timeout_val = max_val
        ax1.plot([timeout_val, timeout_val], [min_val, timeout_val], c=c_other_points, linestyle=":")
        ax1.plot([min_val, timeout_val], [timeout_val, timeout_val], c=c_other_points, linestyle=":")
    
        if debug:
            # debug option
            ax1.scatter(x_data, y_data, marker="o", facecolor="", s=50, label="original data")
    
        # Set axes scale and limits
        ax1.set_xscale("log")
        ax1.set_yscale("log")
    
        # Set axes labels
        ax1.set_xlabel(labels[0])
        ax1.set_ylabel(labels[1])
    
        new_ticks_x = ax1.get_xticks()
        new_ticks_x = new_ticks_x[:-2]
        new_ticks_label = list(new_ticks_x)
        for l_idx in range(len(new_ticks_label)):
            # change 1x10^2 to 100
            if new_ticks_label[l_idx] >= 1:
                new_ticks_label[l_idx] = int(new_ticks_label[l_idx])
        new_ticks_label.append("timeout")
        ax1.set_xticklabels(new_ticks_label)  # , rotation=45)
        ax1.set_yticklabels(new_ticks_label)  # , rotation=45)
    
        if debug:
            # Plot legend
            leg = ax1.legend(loc='best', fancybox=True)
            leg.get_frame().set_alpha(0.5)
    
        # Remove top and right line
        # spines_to_remove = ['top', 'right']
        # for spine in spines_to_remove:
        #     ax1.spines[spine].set_visible(False)
        # ax1.get_xaxis().tick_bottom()
        # ax1.get_yaxis().tick_left()
    
        # Save or show figure
        tight_layout()
        subplots_adjust(top=0.85)
    
        ax1.set_autoscale_on(False)
        ax1.set_xlim([min_val, max_val*2])
        ax1.set_ylim([min_val, max_val*2])
        
        if save != "":
            savefig(save, dpi=100, facecolor='w', edgecolor='w',
                    orientation='portrait', papertype=None,
                    transparent=False, pad_inches=0.1, format='pdf')
        else:
            show()
            
            
            