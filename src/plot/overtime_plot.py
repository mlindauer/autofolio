'''
Created on Jan 13, 2015

@author: Marius Lindauer
'''

import sys
import random
import logging
from numpy import percentile
from numpy import mean
from numpy import arange
import numpy as np
import matplotlib.pyplot as plt
import traceback

class OverTimePlot(object):
    '''
        generates plots of performance over time
    '''


    
    def __init__(self):
        ''' constructor '''
        _, self.ax = plt.subplots()
        
        plt.ylabel("Mean Performance")
        plt.xlabel("Time (s)")
        #plt.xscale("log",basex=2)
        #plt.yscale("log",basex=2)
        plt.xlim(xmin=512,xmax=172800)
        
        self._max = 0
        self._min = sys.maxint
        
        self.medians = None
        self.q25 = None
        self.q75 = None
        self.times = None
        
    def main(self, trajectorys, out_, iqr=True, boot=[12,6,3], yshift=0, yscale=1):
        '''
            main method
            Arguments:
                trajectorys: [[(time,perf),..],..] (cv-fold of repetitions)
                out_: output file
                iqr: boolean, true: plot quantiles
                boot: list of number of bootstrapped samples (simulated number of repetitions)
                yshift: shift on y axis
                yscale: scaling on y axis
        '''
        #if boot:
        for b in boot:
            logging.debug("Boot strapped: %d" %(b))
            self.plot_bootstrapping(trajectorys, b, yshift, yscale, out_)
        #elif iqr:
        #    self.plot_iqr(trajectorys, yshift, yscale, out_)
        #else:
        #    self.plot(trajectorys, yshift, yscale, out_)
            
        plt.tight_layout()
        plt.ylim(ymin=self._min*0.9,ymax=self._max*1.1)
        logging.debug("min: %.2f max:%.2f" %(self._min, self._max))
        plt.legend()
        plt.savefig(out_, format="pdf")
        return self.times, self.medians.tolist(), self.q25.tolist(), self.q75.tolist()
    
    def plot(self, trajectorys, yshift, yscale, out_):
        '''
             plot mean performance over time for each trajectory
             Args:
                 trajectorys: list of trajectorys (time, performance)
                 out_ : file for generated plot (pdf)
        '''
        
        
        index = 1
        for traj in trajectorys:
            x = map(lambda x: x[0] ,traj)
            y = (np.array(map(lambda x: x[1] ,traj)) + yshift) * yscale 
            self.ax.plot(x,y, label="run%d" %(index))
            index += 1
        
        #plt.title("AC - training performance over time")
        #plt.legend(loc=3)
        
    def plot_iqr(self, trajectorys, yshift, yscale, out_, boot=None, min_plot=True):
        '''
            plot median and iqr
            Args:
                 trajectorys: list of trajectorys (time, performance)
                 out_ : file for generated plot (pdf)
                 min_plot: plot minimum per time stamp
        '''
        
        median_trac = self.join_trajectories(trajectorys, 50)
        #print(median_trac)
        q25_trac = self.join_trajectories(trajectorys, 25)  
        q75_trac = self.join_trajectories(trajectorys, 75)
        q0_trac = self.join_trajectories(trajectorys, 0)
        
        times = map(lambda x : x[0], median_trac)
        median = map(lambda x: x[1], median_trac)
        q25 = map(lambda x: x[1], q25_trac)
        q75 = map(lambda x: x[1], q75_trac)
        q0 = map(lambda x: x[1], q0_trac)
        
        #upper_error = [q-m for q,m in zip(q75,median)]
        #lower_error = [m-q for q,m in zip(q25,median)]
        #ax.errorbar(times, median, yerr=[lower_error, upper_error])
        median = (np.array(median) + yshift) * yscale 
        q25 = (np.array(q25) + yshift) * yscale 
        q75 = (np.array(q75) + yshift) * yscale 
        q0 = (np.array(q0) + yshift) * yscale
        if boot:
            graph = self.ax.step(times, median, label="%d configuration runs" %(boot))
        else:
            graph = self.ax.step(times, median)
        self.ax.fill_between(times, q25, q75, alpha=0.3, edgecolor='', facecolor=graph[0].get_color()) #, edgecolor='k')
        if min_plot:
            self.ax.step(times, q0)
            
        for idx, t in enumerate(times):
            if t > 512:
                try:
                    self._max = max(q75[idx-1], self._max)
                except IndexError:
                    pass
                break
        self._min = min(q25[-1], self._min)
        #=======================================================================
        # ax.plot(times, median)
        # ax.plot(times, q25)
        # ax.plot(times, q75)
        #=======================================================================
            
        #plt.title("AC - training performance over time")

        #plt.yscale("log")

        first = True
        for x,t in zip(median, times):
            if t > 86400 and first:
                logging.debug("%.1f: %.1f" %(t, x))
                first = False
                
        if boot == 12: #hack - this should be the number of ac runs 
            self.times = times
            self.medians = median
            self.q25 = q25
            self.q75 = q75
            
    def plot_bootstrapping(self, trajectories_list, samples, yshift, yscale, out_):
        '''
            1. bootstrap samples of trajectories and then min per timestamp
            2. plot_iqr with these new trajectories
        '''
        
        REPS = 100
        new_trajectories_list = []
        for trajectories in trajectories_list:
            new_trajectories = []
            for _ in xrange(REPS):
                sampled_t = []
                for _ in xrange(samples):
                    sampled_t.append(random.choice(trajectories))
                    
                new_traj = self.join_trajectories(sampled_t, quantile=0)
                if new_traj: 
                    new_trajectories.append(new_traj)
                else:
                    logging.error("Empty trajectory during bootstrapping?")
            new_trajectories = self.join_trajectories(new_trajectories, quantile=-1, avg=True)
            new_trajectories_list.append(new_trajectories)
            
        #=======================================================================
        # new_trajectories = []
        # for _ in xrange(REPS):
        #     sub_trajs = []
        #     for sub_traj in new_trajectories_list:
        #         sub_trajs.append(random.choice(sub_traj))
        #     
        #     print(len(sub_trajs))
        #     new_traj = self.join_trajectories(sub_trajs, quantile=-1, avg=True)
        #     new_trajectories.append(new_traj)
        #=======================================================================
            
        self.plot_iqr(new_trajectories_list, yshift, yscale, out_, boot=samples, min_plot=False)
         
    def join_trajectories(self, trajectories, quantile, avg=False):
        '''
            ISSUE: If all trajectories have a length of 1, the function will return an empty trajectory
        '''
        
        indices = [0] * len(trajectories)
        new_traj = []
        first_points = map(lambda x: x[0], trajectories)    
        first_time = min(map(lambda x: x[0], first_points))
        first_min_points = filter(lambda x: x[0] == first_time, first_points)
        first_dists = map(lambda x : x[1], first_min_points)
        if avg:
            new_traj.append((first_time, mean(first_dists)))
        else:
            new_traj.append((first_time, percentile(first_dists, quantile)))
        time_point = first_time    
        
        while True:
            diff_time = []
            for traj, index in zip(trajectories, indices):
                try:
                    diff_time.append(traj[index+1][0] - time_point)
                except IndexError:
                    #traceback.print_exc()
                    diff_time.append(sys.maxint)
            min_time = min(diff_time)
            if min_time == sys.maxint:
                break
            
            for indx in arange(len(diff_time))[np.array(diff_time)==min_time].tolist():
                indices[indx] += 1
            dist = [traj[index][1] for traj, index in zip(trajectories, indices)]
            if avg:
                new_traj.append((min_time, mean(dist)))
            else:
                new_traj.append((min_time, percentile(dist, quantile)))
                
                
        return new_traj