'''
Created on Dec 8, 2014

@author: lindauer
'''

import os
import logging
from subprocess import Popen


class MetaSubmitter(object):
    '''
        submits a job on the meta cluster in Freiburg (SUN GRID ENGINE)
    '''


    def __init__(self, root_path):
        '''
        Constructor
        '''
        self._SUBMIT_SCRIPT = "~/git/metahelper/grid_helper.py"
        self.root_path = root_path
        self._submit_script = os.path.join(root_path, "src", "job_systems", "helpers", "grid_helper.py")
        
    def submit_jobs(self, calls, job_name):
        '''
            submits job on meta with given list of <calls>
        '''
        
        with open("jobs.txt", "w") as fp:
            fp.write("\n".join(calls))
            
        cmd = "python %s -q aad_core.q --lr -o . -l . jobs.txt" %(self._submit_script)
        
        logging.info(cmd)
        p = Popen(cmd, shell=True)
        p.communicate()
        
        