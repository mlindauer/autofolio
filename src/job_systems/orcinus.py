'''
Created on Dec 8, 2014

@author: Marius Lindauer
'''

import logging
from subprocess import Popen

class OrcinusSubmitter(object):
    '''
        submits a job on orcinus as part of the westgrid cluster (Canada) (torque/maui cluster scheduling system)
    '''

    def __init__(self, budget):
        '''
        Constructor
        '''
        self.wallclocktime = budget # + 20 hour for validation
        
    def submit_jobs(self, calls, job_name):
        
        for id, c in enumerate(calls):
            with open("job_%d.pbs" %(id), "w") as fp:         
                fp.write("#!/bin/bash\n")
                fp.write("#PBS -N %s_%d\n" %(job_name, id))
                fp.write("#PBS -j oe\n")
                fp.write("#PBS -r n\n")
                fp.write("#PBS -l walltime=20:00:%d\n" %(self.wallclocktime))
                fp.write("#PBS -l partition=QDR\n")
                fp.write("#PBS -l pmem=4000mb\n")
                fp.write("#PBS -l nodes=1:ppn=2\n\n")
                fp.write("cd $PBS_O_WORKDIR\n")
                fp.write("module load java/1.7.0_71\n")
                fp.write("module load python/2.7.3\n\n")
                
                fp.write(c)
                
            cmd = "qsub job_%d.pbs" %(id)
            logging.info(cmd)
            p = Popen(cmd, shell=True)
            p.communicate()