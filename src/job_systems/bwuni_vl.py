'''
Created on Dec 8, 2014

@author: Marius Lindauer
'''

import logging
from subprocess import Popen


class BWUniVeryLongSubmitter(object):
    '''
        submits a job on bwUni cluster in Baden-Wuerttemberg (moab cluster scheduling system)
    '''


    def __init__(self, budget):
        '''
        Constructor
        '''
        
        self.wallclocktime = budget # + 11 hour for validation
        if self.wallclocktime > 478800:
            logging.error("Too large configuration budget for bwUni Cluster (very long jobs) (<= 478800)")
        
        self.__PROCS_PER_JOB = 8
        
    def submit_jobs(self, calls, job_name):

        for id_ in range(0, len(calls), self.__PROCS_PER_JOB):
            job_calls = calls[id_:id_+self.__PROCS_PER_JOB]
            
            with open("job_%d.pbs" %(id_), "w") as fp:         
                fp.write("#!/bin/bash\n")
                fp.write("#MSUB -N %s_%d\n" %(job_name, id_))
                fp.write("#MSUB -j oe\n")
                fp.write("#MSUB -r n\n")
                fp.write("#MSUB -q verylong\n")
                fp.write("#MSUB -l nodes=1:ppn=%d\n\n" %(min(self.__PROCS_PER_JOB, len(job_calls))))
                fp.write("#MSUB -l walltime=11:00:%d\n" %(self.wallclocktime))
                fp.write("cd $MOAB_SUBMITDIR\n")
                fp.write("module load devel/python/2.7.6\n\n")
                for c in job_calls:
                    fp.write("%s & \n" %(c))
                fp.write('''for job in `jobs -p`
do 
  wait $job
done
                ''')
                
            cmd = "msub job_%d.pbs" %(id_)
            logging.info(cmd)
            p = Popen(cmd, shell=True)
            p.communicate()
                
                    
