# autofolio 2.0
An tool to configure algorithm selectors, such as, claspfolio 2.

## LICENSE
  It is distributed under the GNU Public Licens. See COPYING for
  details regarding the license.
  
## OVERVIEW
  autofolio is an algorithm configuration tool for algorithm selectors. 
  It is based on the ASlib format to read data and assess performance of selectors.
  
  autofolio is written in Python (Serie 2.7) and uses external configurators, e.g., SMAC.
  
  Detailed information, source code, are available at: http://aclib.net/autofolio/

## REQUIREMENTS:

  * SMAC (aclib.net/smac/):
    * Java
  * claspfolio:
    * Python 2.7
    * numpy, liac-arff and scikit learn (tested with 0.14.1; http://scikit-learn.org/dev/index.html):
       * for example: 
        pip install -U scikit-learn
       * Requirements of scikit learn: 
        sudo apt-get install build-essential python-dev python-numpy python-setuptools python-scipy libatlas-dev libatlas3-base
    * to use "--approach ASPEED" or "--aspeed-opt", you need to have executable binaries of gringo (Serie 3), clasp (Serie 2.1) 
      and the runsolver (http://www.cril.univ-artois.fr/~roussel/runsolver/) in ./binaries
      In addition, write permissions are needed in the current directory (".")
  
## USAGE
  autofolio needs as an input an algorithm selection scenario in ASlib format of the COSEAL group. 

    python-2.7 src/autofolio.py -s <ASlib DIR>
    
  For further parameters, call:

    python-2.7 src/autofolio.py -h

## CONTACT:
 	Marius Lindauer 
 	manju@cs.uni-potsdam.de
 	
Supervisors: Holger Hoos, Frank Hutter, Torsten Schaub

