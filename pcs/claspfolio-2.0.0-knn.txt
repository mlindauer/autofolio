# claspfolio-2.0.0 parameter file
# *** NOTE: *** works only with revision clasp-2.1.0 or higher!
# F: flag (yes = flag set; no = flag removed)
# S: skip (only to model constraints for the parameters; won't be given to
# clasp)
# :[String]: will not be parsed for clasp (only syntatic sugar for the parameter file)
# :[int]: alignment of complex parameters
# i : integer range
# l : logarithmic transformation (n > 0)
# ---
@1:separator {W}[W] 
@1:solver {selectors/claspfolio-2/src/trainer/main_train.py}[selectors/claspfolio-2/src/trainer/main_train.py]
# ---
@1:approach {kNN, SBS}[kNN]
@1:max-feature-time [1,600][60]il
# Performance Preprocessing
@1:approx-weights {None,max,rmsd,nrmsd}[None]
@1:performance_trans {None,log,zscore}[None]
@1:contr-filter [0.0,0.32][0.0]
#normalization
@1:normalize {zscore,linear,dec,pca,none}[zscore]
@1:pca-dims[2,12][7]i
@1:impute {none, median, mean, most_frequent}[none]
@1:kNN [1,128][1]il
#--aspeed-opt
@1:F:aspeed-opt {no,yes}[yes]
@1:F:concentrate {yes,no}[no]
@1:max-solver [1,100][3]il
@1:opt-mode {1,2,3,4,5,6}[1]
@1:aspeed:time-limit {300}[300] # maximal time for grounding and solving
@1:time-pre-solvers [1,2048][256]il

@1:pca-dims | @1:normalize in {pca}
@1:kNN | @1:approach in {kNN}
@1:F:concentrate | @1:F:aspeed-opt in {yes}
@1:max-solver | @1:F:aspeed-opt in {yes}
@1:opt-mode | @1:F:aspeed-opt in {yes}
@1:time-pre-solvers | @1:F:aspeed-opt in {yes}
